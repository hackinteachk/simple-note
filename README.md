#Simple Note

-
##### Project site : [Link](https://simple-note-333da.firebaseapp.com)

This project is created using ReactJS and Google Firebase.



#### Run locally
-
```
> yarn install
> yarn start
```

#### Build and Deploy
-
Note: when deploy, choose Realtime Database and Hosting. Enter ***build*** as a public directory.

```
> npm install -g firebase-tools
> yarn build
> firebase login
> firebase init
> firebase deploy
```
Done :)

##### For Firebase Refs
[Link](https://firebase.google.com/docs/reference/js/firebase.auth.Auth#createUserWithEmailAndPassword)
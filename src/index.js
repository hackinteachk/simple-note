import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import {BrowserRouter} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import './index.css'
import Favicon from 'react-favicon';
import img from './img/favi.png'


ReactDOM.render(
  <BrowserRouter>
    <div>
      <Favicon url={`${img}`}/>
      <App/>
    </div>
  </BrowserRouter>,
  document.getElementById('root')
);
registerServiceWorker();

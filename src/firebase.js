import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyAoLBflpLZ0-IJbJ061UCcktry4HqHOalc",
  authDomain: "simple-note-333da.firebaseapp.com",
  databaseURL: "https://simple-note-333da.firebaseio.com",
  projectId: "simple-note-333da",
  storageBucket: "simple-note-333da.appspot.com",
  messagingSenderId: "419647356373"
};

firebase.initializeApp(config);

export default firebase;
export const db = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
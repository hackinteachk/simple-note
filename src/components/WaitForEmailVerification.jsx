import React, {Component} from 'react';
import {auth} from '../firebase';
import {Button} from "material-ui";

export default class WaitingPage extends Component {
  state = {
    verified: false,
  };

  componentWillMount(){
    this.setState({
      verified: auth.currentUser.emailVerified,
    })
  }

  checkVerification = () => {
    if (auth.currentUser.emailVerified) {
      this.props.history.push("/");
    }
  };

  sendEmail = () => {
    auth.currentUser.sendEmailVerification()
      .then(()=> alert("Email resent"))
      .catch(err => alert(err));
  };

  render() {
    return (
      <div>
        <h1>
          Please verify your email first.
        </h1>
        <Button variant="raised" color="secondary" onClick={this.checkVerification}>Verify</Button>
        <p>Didn't receive the verification email? <Button color="primary" onClick={this.sendEmail}>Re-send email</Button></p>
      </div>
    )
  }
}
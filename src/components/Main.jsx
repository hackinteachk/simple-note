import React, {Component} from 'react';
import {auth, db} from '../firebase';
import {withStyles} from 'material-ui/styles';
import Promise from 'bluebird';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import {withRouter} from "react-router-dom";
import DropList from "./DropList";


const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    minWidth: '400px',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  list: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    maxWidth: 360,
    maxHeight: 200,
    overflow: 'auto',
  },

});

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: [],
      current: ""
    };
    this.addNote = this.addNote.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    const uid = auth.currentUser.uid;

    if (!auth.currentUser.emailVerified && auth.currentUser.providerId === 'password') {
      this.props.history.push("/email_verify")
    }

    let notesRef = db.ref('notes/' + uid).limitToLast(100);

    // Add new note
    notesRef.on('child_added', snapshot => {
      let note = {text: snapshot.val(), id: snapshot.key};
      this.setState({
        notes: [note].concat(this.state.notes)
      });
    });

    // Delete Note
    notesRef.on('child_removed', snapshot => {
      let note = {text: snapshot.val(), id: snapshot.key};
      this.setState(
        prevState => ({
          notes: prevState.notes.filter(
            el => el.id !== note.id
          )
        })
      );
    });
  }

  onDragEnd = (result) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const notes = reorder(
      this.state.notes,
      result.source.index,
      result.destination.index
    );

    this.setState({
      notes,
    }, () => {
      // console.log(this.state.notes);
      this.replaceNotes();
    });

  };

  replaceNotes = () => {
    const uid = auth.currentUser.uid;
    let notesRef = db.ref('notes/' + uid);
    const {notes} = this.state;
    Promise.each(notes, n => {
      notesRef.child(n.id).remove();
    })
    Promise.each(notes.reverse(), n => {
      notesRef.push(n.text);
    })
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  addNote(e) {
    e.preventDefault();
    const uid = auth.currentUser.uid;
    db.ref('notes/' + uid).push(this.state.current);
    this.setState({
      current: ""
    });
  }

  removeNote = note => e => {
    e.preventDefault();
    const uid = auth.currentUser.uid;
    db.ref('notes/' + uid).child(note).remove();
  };

  render() {
    const {notes} = this.state;
    const classes = this.props.classes;
    const verified = auth.currentUser.providerData[0].providerId === 'password' ? auth.currentUser.emailVerified : true;
    const email = auth.currentUser.providerData[0].providerId === 'password'
    return (
      <div>
        <p>Hello, {auth.currentUser.displayName !== '' ? auth.currentUser.displayName : "User"}
        <br/>
        <br/>
          {email && (verified ? "Verified" : "Unverified")}</p>
        <br/>
        <Grid container spacing={24}>
          <Grid item xs={4}/>
          <Grid item xs={4}>
            <DropList notes={notes} onDragEnd={this.onDragEnd} removeNote={this.removeNote}/>
          </Grid>
          <Grid item xs={4}/>
        </Grid>
        <form onSubmit={this.addNote}>
          <TextField
            id="note"
            label="Enter new note"
            className={classes.textField}
            value={this.state.current}
            onChange={this.handleChange('current')}
            margin="normal"
          />
          <br/>
          <Button variant="raised" color="primary" type="submit">Add</Button>
        </form>
      </div>
    );
  }
}

export default withRouter(withStyles(styles)(Main));
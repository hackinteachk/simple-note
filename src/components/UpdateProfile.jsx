import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import {Divider, Grid, TextField, Typography} from "material-ui";
import {withRouter} from "react-router-dom";
import {auth} from "../firebase";
import firebase from 'firebase';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
  typo: {
    margin: '0.7em',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  btn: {
    margin: '0.5em',
  },
  textField: {
    marginTop: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  list: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    maxWidth: 500,
    maxHeight: 200,
    overflow: 'auto',
  },

});

class UpdateProfile extends Component {
  state = {
    email: '',
    dpName: '',
    photoUrl: '',
    phone: '',
    curEmail: '',
    curName: '',
    curPhone: '',
    confirmpasswd: '',
    newpasswd: '',
    cnewpasswd: '',
  };

  componentWillMount() {
    const user = auth.currentUser;
    this.setState({
      email: user.email,
      dpName: user.displayName,
      photoUrl: user.photoURL,
      phone: user.phoneNumber,
    })
  }

  handleEmail = () => {
    const user = auth.currentUser;
    const {dpName, curName, confirmpasswd, newpasswd, cnewpasswd} = this.state;
    const credential = firebase.auth.EmailAuthProvider.credential(
      user.email, confirmpasswd
    );
    user.reauthenticateWithCredential(credential).then(
      () => {
        // user.updateEmail(curEmail !== '' ? curEmail : email)
        user.updateProfile({
          displayName: curName !== '' ? curName : dpName,
        });

        if (newpasswd !== '') {
          if (newpasswd !== cnewpasswd) {
            alert("New password mismatch")
          } else {
            user.updatePassword(newpasswd);
          }
        } else {
          this.props.history.push("/profile");
        }
      }
    ).catch(err => alert(err));
  }

  handleSubmit = () => {
    const user = auth.currentUser;
    const pid = user.providerData[0].providerId;
    const {curName, dpName} = this.state;
    if (pid === 'password') {
      this.handleEmail();
    } else {
      user.updateProfile({
        displayName: curName !== '' ? curName : dpName,
      }).then(() => this.props.history.push("/profile"))
    }
  }


  handleInputChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const {dpName, curName, confirmpasswd, newpasswd, cnewpasswd} = this.state;
    const hic = this.handleInputChange;
    const hs = this.handleSubmit;
    const {classes} = this.props;
    const disabled = auth.currentUser.providerData[0].providerId !== 'password';
    return (
      <Grid container className={classes.container}>
        <Grid item xs={4}/>
        <Grid item xs={4}>
          <Typography variant="title" color="inherit" noWrap className={classes.typo}>
            Update Profile
          </Typography>

          <TextField
            id="new dpName"
            label="Display Name"
            className={classes.textField}
            placeholder={dpName}
            value={curName}
            onChange={hic('curName')}
            margin="normal"
          />
          <Divider/>
          <TextField
            id="new pwd"
            label="New Password"
            className={classes.textField}
            value={newpasswd}
            onChange={hic('newpasswd')}
            margin="normal"
            type={"password"}
            disabled={disabled}
          />
          <Divider/>
          <TextField
            id="confirm new pwd"
            label="Confirm New Password"
            placholder={"Type it again"}
            className={classes.textField}
            value={cnewpasswd}
            onChange={hic('cnewpasswd')}
            margin="normal"
            type={"password"}
            disabled={disabled}
          />
          <Divider/>
          <TextField
            id="ConfirmProfileUpdate"
            label="Confirm your changes"
            className={classes.textField}
            value={confirmpasswd}
            onChange={hic('confirmpasswd')}
            margin="normal"
            type={"password"}
            disabled={disabled}
          />
          <Divider/>
          <Button className={classes.btn} variant="raised" color="primary" onClick={() => hs()}>Save</Button>
          <Button className={classes.btn} variant="raised" color="secondary"
                  onClick={() => this.props.history.push('/profile')}>Cancel</Button>
          <Grid item xs={4}/>
        </Grid>
      </Grid>
    )
  }

}

export default withRouter(withStyles(styles)(UpdateProfile));
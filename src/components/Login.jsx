import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {auth} from '../firebase';

import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import firebase from 'firebase';
import {Typography} from "material-ui";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  link: {
    margin: '1em',
  },
  grid: {
    margin: '1em',
  },
  logoLogin: {
    '&:hover': {
      cursor: 'pointer',
    },
    margin: '1em',
  },
  button: {
    margin: theme.spacing.unit,
  }
});

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    auth.onAuthStateChanged(
      user => {
        if (user) {
          //console.log(user);
          this.props.history.push('/')
        }
      }
    )
  }

  onSubmit(event) {
    event.preventDefault();
    const {email, password} = this.state;
    auth.signInWithEmailAndPassword(email, password)
      .then(() => {
        auth.setPersistence(firebase.auth.Auth.Persistence.SESSION)
          .then(() => {
            this.props.history.push('/');
          })
      })
      .catch(authError => {
        alert(authError);
      })
  }


  googleLogin = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider)
      .catch(err => alert(err));
  };

  facebookLogin = () => {
    const pvd = new firebase.auth.FacebookAuthProvider();
    auth.signInWithPopup(pvd)
      .then(result => {

        const oriPurl = result.user.photoURL;
        const purl = oriPurl.include("?width=500") ? oriPurl : oriPurl + "?width=500";
        auth.currentUser.updateProfile({
          photoURL: purl,
        }).catch(err => alert(err));
      })
      .catch(err => alert(err));
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const {email, password} = this.state;
    const classes = this.props.classes;
    return (
      <Grid container>
        <Grid item xs={12}>
          <Typography variant={"title"}>
            Sign In
          </Typography>
          <form onSubmit={this.onSubmit} autoComplete="off">
            <TextField
              id="email"
              label="Email"
              className={classes.textField}
              value={email}
              onChange={this.handleChange('email')}
              margin="normal"
              type="email"
            />
            <br/>
            <TextField
              id="password"
              label="Password"
              className={classes.textField}
              value={password}
              onChange={this.handleChange('password')}
              margin="normal"
              type="password"
            />
            <br/>
            <Button variant="raised" color="primary" type="submit" className={classes.button}>Log in</Button>
            <Button variant={"raised"} color={"secondary"} className={classes.button}
                    onClick={() => this.props.history.push('/signup')}>Sign up</Button>
          </form>
          <Grid container>
            <Grid item xs={4}/>
            <Grid item xs={4} className={classes.grid}>
              <Link to="/forget_pwd" className={classes.link}> Forget Password </Link>
            </Grid>
            <Grid item xs={4}/>
          </Grid>
          <Grid container>
            <Grid item xs={4}/>
            <Grid item xs={4}>
              <img src={require('../img/fb-icon.png')} width={"30px"} onClick={this.facebookLogin}
                   className={classes.logoLogin} alt={"fb-login"}/>
              <img src={require('../img/ggl-icon.png')} width={"30px"} onClick={this.googleLogin}
                   className={classes.logoLogin} alt={"google-login"}/>
            </Grid>
            <Grid item xs={4}/>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(Login);
import React, {Component} from 'react';
import {Route, withRouter} from 'react-router-dom';
import {auth} from '../firebase';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import classNames from 'classnames';

import Background from '../img/bg.jpg'

import {createMuiTheme, MuiThemeProvider} from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import {CircularProgress} from 'material-ui/Progress';
import Button from 'material-ui/Button';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import PrivateRoute from './PrivateRoute';
import Main from './Main';
import Login from './Login';
import Signup from './Signup';
import ForgetPwd from "./ForgetPwd";
import Profile from "./Profile";
import UpdateProfile from './UpdateProfile'
import {Divider, Drawer, Grid, ListItemText, MenuItem, MenuList, Paper, withStyles} from "material-ui";

const theme = createMuiTheme();

const MyList = (props) => {
  const {name, link, classes, close, history} = props;
  return (
    <div>
      <MenuItem className={classes.menuItem}>
        <ListItemText classes={{primary: classes.primary}} onClick={() => {
          close();
          history.push(link);
        }} inset primary={name}/>
      </MenuItem>
      <Divider/>
    </div>
  )
};

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    // backgroundColor: '#414142',
    backgroundImage: `url(${Background})`,
    backgroundSize: 'cover',
  },
  menuItem: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& $primary, & $icon': {
        color: theme.palette.common.white,
      },
    },
  },
  primary: {},
  icon: {},
  appFrame: {
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  listItem: {
    margin: '1em',
  },
  appBar: {
    position: 'absolute',
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'appBarShift-left': {
    marginLeft: drawerWidth,
  },
  'appBarShift-right': {
    marginRight: drawerWidth,
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    padding: '1em',
    position: 'relative',
    width: drawerWidth,
    textAlign: 'center'
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    // backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    // backgroundColor: '#414142',
  },
  'content-left': {
    marginLeft: -drawerWidth,
  },
  'content-right': {
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'contentShift-left': {
    marginLeft: 0,
  },
  'contentShift-right': {
    marginRight: 0,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  typo: {
    margin: '1em',
  },
  btn: {
    margin: '1em'
  },
  profilePic: {
    borderRadius: "50%",
  }
});


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      authenticated: false,
      currentUser: null,
      verified: null,
      open: false,
      anchor: 'left',
      photoUrl: '',
    };
  }

  componentWillMount() {


    auth.onAuthStateChanged(user => {
      if (user) {
        // console.log(auth.currentUser.providerId);
        const provider = auth.currentUser.providerData[0].providerId;
        const fb = provider === 'facebook.com'
        let photoUrl = auth.currentUser.photoURL;
        if(fb){
          photoUrl = photoUrl.includes("?width=500") ? photoUrl : photoUrl + '?width=500'
        }
        this.setState({
            authenticated: true,
            currentUser: user,
            loading: false,
            photoUrl : photoUrl,
            verified: auth.currentUser.providerData[0].providerId === 'password' ? auth.currentUser.emailVerified : true,
          }
        )
      } else {
        this.setState({
          authenticated: false,
          currentUser: null,
          loading: false,
          verified: null,
          photoUrl: '',
        });
      }
    });
  }

  handleDrawerOpen = () => {
    this.setState({open: true});
  };

  handleDrawerClose = () => {
    this.setState({open: false});
  };

  signout = () => {
    auth.signOut()
      .catch(err => alert(err));
    this.setState({
        authenticated: false,
        currentUser: null,
        loading: false,
        open:false,
      },
      () => this.props.history.push("/login"))
  };

  render() {
    const {authenticated, loading} = this.state;
    const {classes, ...rest} = this.props;
    const {anchor, open} = this.state;
    const content = loading ? (
      <div align="center">
        <CircularProgress size={80} thickness={5}/>
      </div>
    ) : (
      <div>
        <PrivateRoute
          exact
          path="/"
          component={Main}
          authenticated={authenticated}
        />
        <PrivateRoute
          exact
          path="/profile"
          component={Profile}
          authenticated={authenticated}
        />
        <PrivateRoute
          exact
          path="/updateProfile"
          component={UpdateProfile}
          authenticated={authenticated}
        />
        <Route exact path="/login" component={Login}/>
        <Route exact path="/signup" component={Signup}/>
        <Route exact path="/forget_pwd" component={ForgetPwd}/>
      </div>
    );

    const {photoUrl} = this.state;
    const drawer = <Drawer
      variant="persistent"
      anchor={anchor}
      open={open}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={this.handleDrawerClose}>
          {theme.direction === 'rtl' ? <ChevronRightIcon/> : <ChevronLeftIcon/>}
        </IconButton>
        <br/>
      </div>
      <div className={classes.toolbar}/>
      <Grid container>
        <Grid item xs>
          <img className={classes.profilePic} src={photoUrl !== null ? photoUrl : require('./usr_icon.png')} height={"150px"} width={"150px"}
               alt={"profile pic"}/>
          <MenuList>
            <MyList name="Note" link="/" classes={classes} close={this.handleDrawerClose} {...rest}/>
            <MyList name="Profile" link="/profile" classes={classes} close={this.handleDrawerClose} {...rest}/>
          </MenuList>
          <Button variant="raised" color="default" onClick={this.signout} className={classes.btn}>Log out</Button>
        </Grid>
      </Grid>
    </Drawer>

    return (
      <MuiThemeProvider theme={theme}>
        <div className={classes.root}>
          <div className={classes.appFrame}>
            <AppBar position="absolute" style={{ position: "fixed"}} color="default" className={classNames(classes.appBar, {
              [classes.appBarShift]: open,
              [classes[`appBarShift-${anchor}`]]: open,
            })}>
              <Toolbar disableGutters={!open}>
                {authenticated &&
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  onClick={this.handleDrawerOpen}
                  className={classNames(classes.menuButton, open && classes.hide)}
                >
                  <MenuIcon/>
                </IconButton>
                }
                <Typography variant="title" color="inherit" noWrap className={classes.typo}>
                  Simple Note
                </Typography>
              </Toolbar>
            </AppBar>
            {drawer}
            <main
              className={classNames(classes.content, classes[`content-${anchor}`], {
                [classes.contentShift]: open,
                [classes[`contentShift-${anchor}`]]: open,
              })}
            >
              <div className={classes.drawerHeader}/>
              <Grid container>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    {content}
                  </Paper>
                </Grid>
              </Grid>

            </main>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};


export default withStyles(styles, {withTheme: true})(withRouter(App));
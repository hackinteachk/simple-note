import React, {Component} from 'react';
import {auth} from '../firebase';

import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import {Typography} from "material-ui";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  btn: {
    margin: '0.5em',
  },
  typo: {
    marginTop: '0.5em'
  }
});

class Signup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email : "",
      password : ""
    }
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    const { email, password } = this.state;
    auth.createUserWithEmailAndPassword(email, password)
      .then(() => {
        auth.currentUser.sendEmailVerification()
          .then(() => {
            alert("Verification email is sent, please confirm your account.");
          })
          .catch(sendEmailError => alert(sendEmailError))
      })
      .catch(authError => {
        alert(authError);
      })
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { email, password } = this.state;
    const classes = this.props.classes;
    return (
      <div>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" color="inherit" noWrap className={classes.typo}>
              Sign Up
            </Typography>
              <form onSubmit={this.onSubmit} autoComplete="off">
                <TextField
                  id="email"
                  label="Email"
                  className={classes.textField}
                  value={email}
                  onChange={this.handleChange('email')}
                  margin="normal"
                  type="email"
                />
                <br />
                <TextField
                  id="password"
                  label="Password"
                  className={classes.textField}
                  value={password}
                  onChange={this.handleChange('password')}
                  margin="normal"
                  type="password"
                />
                <br />
                <Button variant="raised" color="primary" type="submit" className={classes.btn}>Sign up</Button>
                <Button variant="raised" color="secondary" onClick={()=> this.props.history.push("/login")} className={classes.btn}>Cancel</Button>
              </form>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Signup);
import React, {Component} from 'react';
import {auth} from '../firebase';
import {Button, Typography, withStyles} from "material-ui";
import {withRouter} from "react-router-dom";


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  list: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    maxWidth: 500,
    maxHeight: 200,
    overflow: 'auto',
  },

});

class Profile extends Component{
  state = {
    email: '',
    dpName: '',
    photoUrl: '',
    phone: ''
  };

  componentWillMount() {
    const user = auth.currentUser;
    this.setState({
      email: user.email,
      dpName: user.displayName,
      photoUrl: user.photoURL,
      phone: user.phoneNumber,
    })
  }

  handleChange = (key) => e => {
    this.setState({
      [key]: e.target.value,
    })
  }

  render() {
    const {email, dpName, photoUrl, phone} = this.state;
    const {classes} = this.props;
    return (
      <div>
        <Typography variant="title" color="inherit" noWrap className={classes.typo}>
          User Profile
        </Typography>
        <img src={photoUrl !== null ? photoUrl : require('./usr_icon.png')} height={"200px"} width={"200px"}
             alt={"profile pic"}/>
        <br/>
        <p>Display Name: {dpName}</p>
        <br/>
        <p>Email: {email}</p>
        <br/>
        <p>Phone: {phone}</p>
        <br/>

        < Button
          variant="raised"
          color="primary"
          onClick={() => {
            this.props.history.push('/updateProfile')
          }
          }>
          Update Profile
        </Button>
      </div>
  )
  }

  }

  export default withRouter(withStyles(styles)(Profile));
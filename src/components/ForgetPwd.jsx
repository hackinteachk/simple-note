import React, {Component} from 'react';
import {auth} from '../firebase';
import {Button, Grid, Paper, TextField, withStyles} from "material-ui";
import {withRouter} from "react-router-dom";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class ForgetPwd extends Component {
  state = {
    email: "",
  };

  handleChange = value => e => {
    e.preventDefault();
    this.setState({
      [value]: e.target.value,
    })
  };

  onSubmit = (e) => {
    e.preventDefault();
    const {email} = this.state;
    auth.sendPasswordResetEmail(email)
      .then(() => alert("Reset password email sent!"))
      .catch(err => alert(err));
  };

  render() {
    const {email} = this.state;
    const {classes} = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <h1>Password Reset</h1>
            <form onSubmit={this.onSubmit} autoComplete="false">
              <TextField
                id="rEmail"
                label="Email"
                className={classes.textField}
                value={email}
                onChange={this.handleChange('email')}
                margin="normal"
              />
              <br/>
              <Button variant="raised" color="secondary" onClick={() => this.props.history.push("/login")}>Back</Button>

              <Button variant="raised" color="primary" type="submit">Submit</Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

export default withStyles(styles)(withRouter(ForgetPwd));